### Dependencies

- Core Utilities
- [jq](https://stedolan.github.io/jq/)

### Use

```
git clone https://gitlab.com/chrisbohn8/enbw-blg.git
cd enbw-blg
./enbw_blg
```

After the scripts runs view the resulting beer_list.html page in your favorite web browser.

### To Do's
- create printable beerlist
